package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"time"
)

//

var url = "http://localhost:3001/api/timing/"

func main() {

	dummydata := readCSV()

	for _, dummy := range dummydata {

		datacorridor, err := dummyDataTimingPoint(dummy, "corridor")
		if err != nil {
			log.Println(err)
		}
		fmt.Println(dummy)
		httpRequest(datacorridor)
		randomDuration()

		datafinish, err := dummyDataTimingPoint(dummy, "finish")
		if err != nil {
			log.Println(err)
		}
		fmt.Println(dummy)
		httpRequest(datafinish)
		randomDuration()

	}
}

func randomDuration() {
	randomizer := rand.Intn(5)
	time.Sleep(time.Second * time.Duration(randomizer))
}

func dummyDataTimingPoint(c contestant, timingPoint string) ([]byte, error) {
	t := time.Now().Format("15:04:05.0")
	c.Time = t
	c.Timing = timingPoint
	return json.Marshal(c)
}

func httpRequest(json []byte) []byte {
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(json))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		log.Fatal(err)
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()
	return body
}
