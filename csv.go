package main

import (
	"encoding/csv"
	"io"
	"log"
	"os"
	"strconv"
)

type contestant struct {
	ChipID int    `json:"chip_id,omitempty"`
	Timing string `json:"timing_point,omitempty"`
	Time   string `json:"time,omitempty"`
}

func readCSV() map[int]contestant {
	contestants := make(map[int]contestant)
	file, err := os.Open("testdata/athletes.csv")
	if err != nil {
		log.Println(err)
	}
	defer file.Close()
	w := csv.NewReader(file)

	for {
		line, err := w.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		} else {
			id, err := strconv.Atoi(line[0])
			if err != nil {
				log.Println(err)
			}
			a := contestant{
				ChipID: id,
			}
			contestants[a.ChipID] = a

		}
	}
	return contestants
}
