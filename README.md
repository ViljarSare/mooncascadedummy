# Mooncascade dummydata client for testing the timingApp

## Requirements
Make sure Golang 1.13 is installed  
Make sure you have Docker 2.1.0 or later installed



## Running
- Navigate to the cloned source code folder with command prompt 
- type 'go build'
- run the executable file by typing ./dummy --port 'portnumber'  
Make sure that the 'portnumber' is corresponding to the running timingApp port(default is 3000)


The client has the csv file located in the /testdata folder.
It reads all the data in and starts to send them one by one to the timingApp.